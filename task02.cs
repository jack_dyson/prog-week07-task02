using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace x
{
    class program
    {
        static void method( )
        {
            Console.WriteLine("This is a method.");
            Console.WriteLine("The second line is now printed to the screen.");
        }
        static void Main(string[] args)
        {
            for (var i = 0; i < 4; i++)
            {
                method();
            }
        }
    }
}